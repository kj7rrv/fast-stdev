import timeit
import statistics
import fast_stdev
import random
import numpy
import sys

print("Python", sys.version, "\n")

iterations = 1000
numbers = [1, 2, 4]
numbers = [random.randint(1, 100) for i in range(1000)]
numbers = [random.random() for i in range(1000)]
np_array = numpy.array(numbers)


def time(code):
    time = round(
        1000000
        * timeit.timeit(
            code,
            number=iterations,
            globals=globals(),
        )
        / iterations
    )

    return "(time: " + (str(time)).rjust(4, " ") + "μs)"


print("Sample standard deviation:")

print(
    "    stdlib     : "
    + str(statistics.stdev(numbers)).ljust(20, " ")
    + time("statistics.stdev(numbers)")
)

print(
    "    fast_stdev : "
    + str(fast_stdev.stdev(numbers)).ljust(20, " ")
    + time("fast_stdev.stdev(numbers)")
)

print(
    "    numpy      : "
    + str(np_array.std(ddof=1)).ljust(20, " ")
    + time("np_array.std(ddof=1)")
)

print("\nPopulation standard deviation:")

print(
    "    stdlib     : "
    + str(statistics.pstdev(numbers)).ljust(20, " ")
    + time("statistics.pstdev(numbers)")
)

print(
    "    fast_stdev : "
    + str(fast_stdev.pstdev(numbers)).ljust(20, " ")
    + time("fast_stdev.pstdev(numbers)")
)

print(
    "    numpy      : "
    + str(np_array.std()).ljust(20, " ")
    + time("np_array.std()")
)
