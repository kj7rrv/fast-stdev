# SPDX-License-Identifier: LGPL-3.0-or-later

import math
from typing import Sequence, Union, List


def _divide_list(
    dividends: Sequence[Union[float, int]], divisor: Union[float, int]
) -> float:
    return sum(dividends) / divisor


def stdev(numbers: Sequence[float]) -> float:
    """Calculate the sample standard deviation of a group of numbers

    Parameters
    ----------
    numbers : list of int or float
        The numbers to calculate the mean of

    Returns
    -------
    float
        The sample standard deviation of the numbers

    """
    mean = _divide_list(numbers, len(numbers))
    squared_deviations = [(mean - i) ** 2 for i in numbers]
    return math.sqrt(_divide_list(squared_deviations, len(numbers) - 1))


def pstdev(numbers: Sequence[float]) -> float:
    """Calculate the population standard deviation of a group of numbers

    Parameters
    ----------
    numbers : list of int or float
        The numbers to calculate the mean of

    Returns
    -------
    float
        The population standard deviation of the numbers

    """
    mean = _divide_list(numbers, len(numbers))
    squared_deviations = [(mean - i) ** 2 for i in numbers]
    return math.sqrt(_divide_list(squared_deviations, len(numbers)))
