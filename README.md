# fast-stdev

Fast(er) standard deviation calculation (compared to the standard library) for
Python [experimental]

    import fast_stdev
    print(fast_stdev.stdev([1, 2, 4])) # Sample standard deviation
    print(fast_stdev.pstdev([1, 2, 4])) # Population standard deviation

Run `test.py` to compare results and run times with the `statistics.[p]stdev`
functions and NumPy's `array.std`.
